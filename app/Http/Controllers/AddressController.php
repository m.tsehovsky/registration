<?php

namespace App\Http\Controllers;


use App\Events\AddressDeleting;
use App\Helpers\Cleaner;
use App\Http\Requests\AddressFormRequest;
use App\Mail\AddressDeletingMailer;
use App\Models\User;
use App\Models\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AddressController extends Controller
{
    private $cleaner;

    public function __construct(Cleaner $cleaner)
    {
        $this->cleaner = $cleaner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $data = User::findOrFail(Auth::user()->id)->information()->orderBy('name')->get();
            if ($data->count() > 0) {

                return response()->json($data);
            }
        }

        $errorMessage[] = 'No one address added yet';
        return response()->json($errorMessage, 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
/*
 * Использовал "observer" для логированя и рассылки при создании адреса
 * */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressFormRequest $request)
    {
        $data = [];

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $data = array(
                'name'          => $this->cleaner->cleanData($request->get('name')),
                'country'       => $this->cleaner->cleanData($request->get('countrySel')),
                'city'          => $this->cleaner->cleanData($request->get('citySel')),
                'street'        => $this->cleaner->cleanData($request->get('street')),
                'home_number'   => $this->cleaner->cleanData($request->get('number')),
                'information'   => $this->cleaner->cleanData($request->get('info')),
            );

            User::findOrFail(Auth::user()->id)->information()->create([
                'name'          => $data['name'],
                'country'       => $data['country'],
                'city'          => $data['city'],
                'street'        => $data['street'],
                'house'         => $data['home_number'],
                'information'   => $data['information'],
            ]);
        }

        $entity = User::findOrFail(Auth::user()->id)->information()->orderBy('name')->get();

        if ($entity->count() > 0) {

            return response()->json($entity);
        } else {
            $errorMessage[] = 'Something bad happened with server! We are solving this problem now!';

            return response()->json($errorMessage, 404);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /*
 * Использовал связку "event->listener" для логированя и рассылки при удалении адреса
 * */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = $this->cleaner->cleanData($id);

        if (Auth::user()->can('delete', UserInformation::find($id))) {
            $entity = User::findOrFail(Auth::user()->id)->information()->where('id', $id)->first();

            if ($entity->count() > 0) {
                $entity->delete();

                $usersInfo                  = UserInformation::withTrashed()->findOrFail($id)->users()->first();

                $deletedRecord              = UserInformation::withTrashed()->where('id', $id)->first();
                $deletedRecord->userName    = $usersInfo->name;
                $deletedRecord->userEmail   = $usersInfo->email;

                event(new AddressDeleting($deletedRecord));

                $status[] = 'success';

                return response()->json($status);
            } else {
                $errorMessage[] = 'Someone already deleted this record!';

                return response()->json($errorMessage, 404);
            }
        } else {
            $errorMessage[] = 'You are not authorised for deleting this record!';

            return response()->json($errorMessage, 401);
        }
    }
}
