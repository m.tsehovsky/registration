<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string',
            'countrySel'    => 'required|not_in:[0,""]',
            'citySel'       => 'required|not_in:[0,""]',
            'street'        => 'required|string|max:50',
            'number'        => 'required|string|max:10',
            'info'          => 'max:250',
//            'name'          => 'required|string|unique:user_information,name',
        ];
    }

    public function messages()
    {
        return [

            'required'      => 'The :attribute field must be filled',
            'string'        => 'The :attribute field must contains only alpha-decimal characters',
            'max'           => 'The :attribute field must contain maximum :max symbols',
//            'unique'        => 'We already have Address Type with this name',

            'countrySel.required'   => 'The Country field must be filled',
            'citySel.required'      => 'The City field must be filled',
            'countrySel.not_in'     => 'The Country field must be filled',
            'citySel.not_in'        => 'The City field must be filled',
        ];
    }
}
