<?php

namespace App\Listeners;

use App\Mail\AddressDeletingMailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AddressDelete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $message = 'Information id=' . $event->deletedRecord->id . ' about user ' . $event->deletedRecord->userName
            . ' (email: ' . $event->deletedRecord->userEmail . ')'
            . ' was deleted by user: ' . Auth::user()->name . ' at: ' . $event->deletedRecord->deleted_at->format('d-m-Y H:i:s');

        Log::channel('address_deleting')->debug($message);

        Mail::to($event->deletedRecord->userEmail)->send(new AddressDeletingMailer($event->deletedRecord));
    }
}
