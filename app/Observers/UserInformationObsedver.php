<?php

namespace App\Observers;

use App\Mail\AddressCreatingMailer;
use App\Models\UserInformation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserInformationObsedver
{
    /**
     * Handle the UserInformation "created" event.
     *
     * @param  \App\Models\UserInformation  $userInformation
     * @return void
     */
    public function created(UserInformation $userInformation)
    {
        $entity =  $userInformation->users()->first();

        $parameters = $userInformation;
        $parameters->userName = $entity->name;
        $parameters->userEmail = $entity->email;

        $message = 'You were added a new address for user ' . $parameters->userName
            . ' (email: ' . $parameters->userEmail . ')'
            . ' by user: ' . Auth::user()->name . ' at: ' . $parameters->created_at->format('d-m-Y H:i:s');

        Log::channel('address_creating')->debug($message);

        Mail::to($parameters->userEmail)->send(new AddressCreatingMailer($parameters));



    }

    /**
     * Handle the UserInformation "updated" event.
     *
     * @param  \App\Models\UserInformation  $userInformation
     * @return void
     */
    public function updated(UserInformation $userInformation)
    {
        //
    }

    /**
     * Handle the UserInformation "deleted" event.
     *
     * @param  \App\Models\UserInformation  $userInformation
     * @return void
     */
    public function deleted(UserInformation $userInformation)
    {
        //
    }

    /**
     * Handle the UserInformation "restored" event.
     *
     * @param  \App\Models\UserInformation  $userInformation
     * @return void
     */
    public function restored(UserInformation $userInformation)
    {
        //
    }

    /**
     * Handle the UserInformation "force deleted" event.
     *
     * @param  \App\Models\UserInformation  $userInformation
     * @return void
     */
    public function forceDeleted(UserInformation $userInformation)
    {
        //
    }
}
