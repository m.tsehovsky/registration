<?php

namespace App\Events;

use App\Http\Controllers\AddressController;
use App\Models\UserInformation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AddressDeleting
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $deletedRecord;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserInformation $deletedRecord)
    {
        $this->deletedRecord = $deletedRecord;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
