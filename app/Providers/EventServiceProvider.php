<?php

namespace App\Providers;

use App\Events\AddressDeleting;
use App\Listeners\AddressDelete;
use App\Models\UserInformation;
use App\Observers\UserInformationObsedver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        AddressDeleting::class => [
            AddressDelete::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        UserInformation::observe(UserInformationObsedver::class);
    }
}
