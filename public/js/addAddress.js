/**
 * Adding a new address and rendering actual addresses without reloading the page
 */

$( document ).ready(function() {
    $('.green_btn').click(
        function(){
            if($('#name').val() && $('#countrySel').val() && $('#citySel').val() && $('#street').val() && $('#number').val()) {
                sendAjaxForm('ajax_form', 'address');
            } else {
                swal('Required fields are empty!', 'Fill in the \'*\' fields, please', 'error');
            }
            return false;
        }
    );
});

function sendAjaxForm(ajax_form, url) {
    $.ajax({
        url         :url,
        type        :'POST',
        data        :$('#'+ajax_form).serialize(),
        dataType    : 'json',
        success: function(response) {
            $('#error_form').empty();
            $('#template').empty();
            $('#ajax_form')[0].reset();
            $('#citySel').val('');

            for (var i = 0; i < response.length; i++) {
                $('#template').append('<div class="item" id="'+ response[i].id + '">' + '<br> '
                    + '<h3>' + response[i].name + '</h3>' + '<p>' + 'Country: ' + response[i].country + '; '
                    + 'City: ' + response[i].city + '; ' + 'Street: ' + response[i].street + '; '
                    + 'Home number: ' + response[i].house + '.</p>'
                    + '<p>' + 'Addition information: ' + response[i].information + '</p>'
                    + '<div class="actbox" > ' + '<a class="bcross" data-id="' + response[i].id + '"></a></div></div>');
            }
            swal('Added!', 'Address created.', 'success');
        },
        error: function(data) {
            var result = data.responseJSON;
            var errors = result['errors'];
            var messages = Object.values(errors);

            if (messages.length > 0) {
                $('#error_form').empty();
                $('#error_form').append('<div>  <h3 style="color: red">Form Error!!!</h3></div> <div><ul>');
                for (let i = 0; i < messages.length; i++) {
                    $('#error_form').append('<li style="color: red">' + messages[i] + '</li>');
                }
                $('#error_form').append('</div></ul>');
            }
        }
    });
}

