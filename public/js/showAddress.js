/**
 * Rendering actual addresses after loading the page
 */

$(document).ready(function () {
    getAddresses('address');

    return false;
});

function getAddresses(url) {
    $.ajax({
        url         :url,
        type        :'GET',
        dataType    : 'json',
        success: function(response) {
            $('#error_form').empty();
            $('#template').empty();
            $('#ajax_form')[0].reset();

            for (var i = 0; i < response.length; i++) {
                $('#template').append('<div class="item" id="'+ response[i].id + '">' + '<br> '
                    + '<h3>' + response[i].name + '</h3>' + '<p>' + 'Country: ' + response[i].country + '; '
                    + 'City: ' + response[i].city + '; ' + 'Street: ' + response[i].street + '; '
                    + 'Home number: ' + response[i].house + '.</p>'
                    + '<p>' + 'Addition information: ' + response[i].information + '</p>'
                    + '<div class="actbox" > ' + '<a class="bcross" data-id="' + response[i].id + '"></a></div></div>');
            }
        },
        error: function(data) {
            var message = data.responseJSON;
            if (message.length > 0) {
                $('#template').empty();
                $('#template').append('<div>  <h3 style="color: red; text-align: center">We are sorry...</h3></div>'
                    + '<div><ul><li style="color: red; text-align: center">' + message + '</li></div></ul>');
            }
        }
    });
}

