/**
 * Removing the selected address from the database and from the view without reloading the page
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$( document ).ready(function() {
    $(document).on('click','.actbox', function () {
            var id = $(this).find('.bcross').attr('data-id');
            dellById(id,  'address/' + id);
        }
    );
});

function dellById(id, url) {
    $.ajax({
        url         : url,
        type        : 'POST',
        data        : {
            'id'    : id,
            '_method': 'DELETE',
        },
        dataType    : 'json',
        success: function() {
            $('#' + id).remove();
            swal('Deleted!', 'Address deleted.', 'success');
        },
        error: function(data) {
            var message = data.responseJSON;
            console.log(message);
            swal('Not Deleted!', 'Something is wrong.', 'error');
            if (message.length > 0) {
                $('#error_form').empty();
                $('#error' + id).empty();
                $('#' + id).append('<div id="error' + id + '"> <div> <h3 style="color: red; text-align: center">We are sorry...</h3></div>'
                    + '<div><ul><li style="color: red; text-align: center">' + message + '</li></div></ul></div>');
            }
        }
    });
}
