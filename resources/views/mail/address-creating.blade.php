<h1>Dear {{ $parameters->userName }}!</h1>
<p>We noticed that you were create a new address at {{ $parameters->created_at->format('d-m-Y H:i:s') }}
    from account {{ $parameters->userEmail }}.</p>
<p>You are welcome:</p>
<ul>
    <li>Address type: {{ $parameters->name }}</li>
    <li>Country: {{ $parameters->country }}</li>
    <li>City: {{ $parameters->city }}</li>
    <li>Street: {{ $parameters->street }}</li>
    <li>House number: {{ $parameters->house }}</li>
    <li>With comment: {{ $parameters->information }}</li>
</ul>
