<h1>Dear {{ $parameters->userName }}!</h1>
<p>We noticed that you were deleted one of yor addresses at {{ $parameters->deleted_at->format('d-m-Y H:i:s') }} from account {{ $parameters->userEmail }}.</p>
<p>Check please, that address no longer needed:</p>
<ul>
    <li>Address type: {{ $parameters->name }}</li>
    <li>Country: {{ $parameters->country }}</li>
    <li>City: {{ $parameters->city }}</li>
    <li>Street: {{ $parameters->street }}</li>
    <li>House number: {{ $parameters->house }}</li>
</ul>
